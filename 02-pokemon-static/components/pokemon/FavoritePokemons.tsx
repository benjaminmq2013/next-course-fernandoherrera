import { Card, Grid } from "@nextui-org/react";
import { FavoriteCardPokemon } from './FavoriteCardPokemon';
import React from 'react';

type Pokemons = {
  favoritePokemons: number[]
}


export const FavoritePokemons:React.FC<Pokemons> = ({ favoritePokemons }) => {
  return (
    
      <Grid.Container gap={2} justify="flex-start">
        {favoritePokemons.map((id) => (
          <FavoriteCardPokemon PokemonId = {id}  key={id}/>
        ))}
    </Grid.Container>
  );
};
