import React from 'react'
import { Grid, Card } from '@nextui-org/react';
import { useRouter } from 'next/router';

type Props = {
    PokemonId: number
}


export const FavoriteCardPokemon: React.FC<Props> = ({ PokemonId }) => {
    const router = useRouter()

    const onFavoriteClicked = ( PokemonId :number) => {
        router.push(`/pokemon/${PokemonId}`)
    }

  return (
    <>
        <Grid xs={6} sm={3} md={2} xl={1} onClick={ () => onFavoriteClicked(PokemonId) }>
            <Card hoverable clickable css={{ padding: 10 }}>
              <Card.Image
                src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${PokemonId}.svg`}
                width={"100%"}
                height={140}
              />
            </Card>
          </Grid>
    </>
  )
}
