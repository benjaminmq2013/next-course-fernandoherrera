import React from 'react'
import Head from 'next/head'
import { Navbar } from "../ui"

type Props = {
    children: JSX.Element,
    title: string
}

const origin = (typeof window === "undefined" ? "" : window.location.origin)

export const Layout: React.FC<Props> = ({children, title}) => {

  
  
  return (
    <>
        <Head>
            <title>{ title || "Pokemon App" }</title>
            <meta name="author" content="Benjamin Martinez"/>
            <meta name="description" content={`Informacion sobre el pokemon ${title}`}/>
            <meta name="keywords" content={`${ title }, Pokemon, Pokedex`}/>

            <meta property="og:title" content={`Información sobre ${ title }`} />
            <meta property="og:description" content="Informacion y datos sobre los pokemon" />
            <meta property="og:image" content={`${origin}/img/pokemon-go.webp`} />
        </Head>

        <Navbar />

        <main style={{
            padding: "0px 20px"
        }}>

        </main>

        <main>{ children }</main>
    </>
  )
}
